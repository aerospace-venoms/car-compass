# Car Compass Project

Inspired by the fact that my vehicle does (somehow!) not include a compass, I embark on this project to design one.

## Design Requirements

* Input will be 12v car aux power (11-14v tolerant)
* Output on a 2-character LED display, in red so as to not spoil night vision
* Be accurate to within 10 degrees of magnetic direction
* Use no more than 500mA of power
* Must be tolerant of as low as -10F and +120F temperatures, since it will live inside a car
* Must have an on/off switch in case it is too bright at night. 

## Hardware

The hardware I have selected for this project includes:

* Adafruit Arduino Metro mini
* Dual alphanumeric red LED display
* 1k ohm resistors for each of the LED segments
* 74HC595 Shift Register
* Triple-axis Accelerometer+Magnetometer (Compass) Board - LSM303

## Design Process

### Initial Prototyping

I have begun the design by protoptying my connections on a breadboard. I plan to chain four shift registers together so that I can drive all 28 output pins (or up to 32 of them) using just three digital output pins on the Arduino.

Once I have this working, I can tune which value of resistor to use for each of the digits. Recommended value is 20mA, which means I will want 280 ohm resistors to start with. I started with my next best resistor, a 470 ohm one, and the display seemed plenty bright to me. Again with a 1k resistor and that seems to also work fine. Ultimately, with a 1k resistor I'm getting just 1mA of power per segment, so I can expect to only need 28mA, which is well within the Arduino regulator's ability to supply.

If I want to run all 24 segments at full power, this will require 28 * 20mA, or 560mA, using 2.8W of power. Metro mini's regulator can supply 150mA maximum, so we are well within power budget, so long as we use 1k resistors on the segments and limit the LED usage to 1mA per segment.

Time to write the program to get a single digit to work. I think I will need to multiplex these displays, though... I will need to sink one of the digits to ground one at a time. Luckily, the Atmega328 can sink up to 40mA per pin (and 200mA total) so we will be well within limits if only driving one digit at a time.

I wrote a simple header file to help me define what pins go to which segments of the LED display, and which combination of segments make up what characters.

### Shift registers

After a bit of fiddling and some wrong wiring, I got my simple shift register code working. I'm happy with the 1mA brightness on the segments, and I will actually only need two shift registers for both digits, assuming I can timing-multiplex them.

I spent a long time trying to debug why the MSB 7 bits of my output weren't working... it was because I was passing an integer into my shift register function. Oops.

Now that I have both digits displaying all of the characters I care about (N, E, S, W) at once, I will need to get them to work independently. In order to do this, I will need to multiplex them - I can do this by changing the state of the common-negative terminal of each digit. 

### Compass integration

Now, I just need to get the compass working. I found an Arduino library from Pololu that allows me to calibrate the compass... here are the values I got from that:

min: {  -573, -488, -520}    max: {  +448, +411, +407}

With all of that accounted for, I now need to subdivide my compass rosette.
Since there are 8 total directions, and 360 degrees in a circle, I will have a total of 45 degrees per angle. If I offset my heading by +12.5 degrees, it will be that much easier to make this work. 

Great, everything is working together nicely! I had to do a little tweaking to get rid of the "ghosting" effect on the multiplexed LEDs, though. I solved it by making sure the LEDs had a ms or so period of being sunk to ground, in order to discharge any parasitic capacitance they had after shutting them off from power. Otherwise, they would have a faint yet incredibly annoying ghost image on the other digit, defeating the point of multiplexing. It worked!

### Compass testing and calibration

After testing in the car, it seems there is a persistent magnetic field in the cabin for some reason. Manually rotating the compass in the cabin works fine, but actually turning the car does not. I'm not sure what it is, but I will try recalibrating to see if I can discover what is going on. 

From that calibration, I got some very different values:
min: {  -977, -1092, -722}    max: {  +819, +801, +1031}
(previous values)
min: {  -573, -488, -520}    max: {  +448, +411, +407}

Hm, maybe this will help? Not sure.

When I did that last calibration, I had rotated the compass around all axes of freedom by hand. It gave some really large values, but this isn't exactly what I wanted. After thinking about it for a bit, I realized that what I wanted to do was actually rotate it about the z axis while fixed inside the vehicle cabin (aka drive in a circle while calibrating). This way, I would get accurate readings for the min and max values accounting for the fixed magnetic field created by the vehicle.

I'm not sure what the source of the fixed magnetic field within the vehicle cabin is, but I suspect it has to do with either dash board wiring or the alternator placement. Investigating that problem is beyond the scope of this project, and I was indeed able to compensate for it by doing a simple calibration inside the cabin.

I should consider adding a "calibrate" function to permit recalibration without connecting to a computer. Perhaps I could simply add a small switch to enable this functionality, saving the calibration values to EEPROM memory for persistence between reboots. That would sure be nifty.

### PCB Design 

The next steps for this project include making a board, and sourcing parts. I have already decided on a bill of materials, so I will need to find appropriate parts for that. I will also need to decide on a cad program for designing my board. I have decided to use KICAD for Linux, so that I can easily include the board files in this project.

In terms of board design, I want the board to mount the compass roughly horizontally. I also will want a second board ju st to hold the LED display at least, so that it is mounted at a 90 degree offset from the rest of the board. I can always 3d print an enclosure to make everything fit together.

I am okay with soldering some surface mount components, and I think it would vastly shrink the size of the finished product do do that, especially with resistors and. I think I want a single "motherboard" with almost everything, and the LCD with its resistors on-board. 

Sourcing parts for the finished design:
SN74HC595 in SOIC for the shift registers
Adafruit Metro for microcontroller, but really any would do...
KWA-541XVB for display
1k resistors: VSOR1603102 $3 ea... so get 4 of them

(...4 weeks later...)

# Finished Board and Wrap-up

Now that I have designed the board, I have several things I would improve for future iterations.

* Avoiding intermediate connectors on the board if at all possible saves a lot of space. Specifically, I mean things like using male/female header pins, when just male header pins would work fine (although then you can't remove the microcontroller from the board later). Also, not using DIP sockets saved lots of space, and being willing to "backpack" boards was good.
* I need to make sure I get the clearance for board/board connections (right angle) correct. I also need to research board/board connector designs. I really liked the 2-wide connectors.
* The vias for my male pin headers were drilled too small to work properly. I just soldered to them anyway, but it added some extra work that I could have avoided pretty easily. 
* I need to find a way to get my circuit fab shop to not leave those stupid little tabs on the sides of my boards. I hate post-processing. 
* I forgot to route a single wire in the board design. It was pretty easy to debug and find, but I realized that I forgot to run my design rules check for the daughterboard. I just "white-wired" that connection without too much trouble, and you can hardly see it in the finished design. 
* I did really enjoy hiding SMD components under other components. It really made the board look sleeker.
* Adding silk screen graphics to the board was very helpful

Finally, the board is done. At long last! Now I simply need to calibrate it for the vehicle once again, and mount it into the cockpit. Ultimately, I will want to 3d print an enclosure for it, and find a low-profile USB micro (or 90 degree) cable.

Calibration:
Once again, I will run the Pololu LSM303 library to calibrate the device. 
