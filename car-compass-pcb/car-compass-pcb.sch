EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74HC595 U?
U 1 1 5F6A4E72
P 2550 2150
F 0 "U?" H 2550 2931 50  0000 C CNN
F 1 "74HC595" H 2550 2840 50  0000 C CNN
F 2 "" H 2550 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 2550 2150 50  0001 C CNN
	1    2550 2150
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC595 U?
U 1 1 5F6A5783
P 3950 2150
F 0 "U?" H 3950 2931 50  0000 C CNN
F 1 "74HC595" H 3950 2840 50  0000 C CNN
F 2 "" H 3950 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 3950 2150 50  0001 C CNN
	1    3950 2150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
