/*
  Car_compass

  Arduino C++ code to implement a car compass for my poor car.
  Maybe it will help me gain a sense of direction.

 */

/* Library includes */
#include "KWA-541CVB.h"
#include <Arduino.h>
#include <LSM303.h>
#include <Wire.h>

/* Pin declarations */

/* I2C pins are A4, A5 */

/* GPIO current sinks for multiplexing the LED digits */
#define pin_digit1 6
#define pin_digit2 5

/* Shift register */
#define pin_output_enable 8 // OE, active low, pin 13 on shift register (SR)
#define pin_master_reset 9  // MR, active low, pin 10 on SR
#define pin_latch 10        // ST_CP, pin 12 on SR
#define pin_clock 11        // SH_CP, pin 11 on SR
#define pin_data 12         // data serial, DS, pin 14 on SR

#define pin_led 13 // built-in Arduino LED for debug

/* Other consts */
const int delay_time = 100;
// const int compass_offset_deg = -90;
const int compass_offset_deg = 0;
LSM303 compass;

// the setup routine runs once when you press reset:
void setup() {

  setup_led_multiplex();
  setup_shift_registers();

  /* Debug LED */
  pinMode(pin_led, OUTPUT);
  digitalWrite(pin_led, LOW);

  /* begin serial comms for debug */
  Serial.begin(115200);

  /* Setup the compass i2c peripheral */
  setup_compass();
}

void loop() {

  // Get the heading:
  compass.read();
  unsigned long heading_chars = get_heading_chars(compass.heading());

  // display the direction
  int flicker_ms = 5; // flicker in ms
  int cooldown_ms = 1;
  unsigned long time = millis();
  while (millis() - time < delay_time) {
    display_digits(0x1, heading_chars >> 16, 16, flicker_ms, cooldown_ms);
    display_digits(0x2, (int)heading_chars, 16, flicker_ms, cooldown_ms);
  }
}

/* ============================ Shift Register ============================ */

void setup_shift_registers(void) {
  /* Setup for shift registers */
  pinMode(pin_output_enable, OUTPUT);
  pinMode(pin_master_reset, OUTPUT);
  pinMode(pin_latch, OUTPUT);
  pinMode(pin_clock, OUTPUT);
  pinMode(pin_data, OUTPUT);
  shift_reset(); // reset all shift register bits and prepare for output
}

void shift_reset(void) {
  // load 0s into shift register
  digitalWrite(pin_master_reset, LOW);
  digitalWrite(pin_output_enable, LOW);

  // reset the storage register
  digitalWrite(pin_latch, LOW);
  digitalWrite(pin_latch, HIGH);
  // prepare to accept data
  digitalWrite(pin_master_reset, HIGH);
}

void shift_out(int to_write, int n) {
  for (int i = 0; i < n; i++) {
    // mask off the bit we are going to write
    int write_bit = (to_write >> (n - 1 - i)) & 0x01;
    digitalWrite(pin_data, write_bit);
    // low->high the clock to shift that bit in
    digitalWrite(pin_clock, LOW);
    digitalWrite(pin_clock, HIGH);
  }
  // clock the latch pin.
  // this will transfer the data from input buff to storage.
  digitalWrite(pin_latch, LOW);
  digitalWrite(pin_latch, HIGH);
}

/* ============================ LED Multiplexing ============================ */

void setup_led_multiplex(void) {
  /* Setup LED display digit current sinks */
  pinMode(pin_digit1, OUTPUT);
  pinMode(pin_digit2, OUTPUT);
  digitalWrite(pin_digit1, LOW);
  digitalWrite(pin_digit2, LOW);
}

void display_digits(int digits_mask, int shift_values, int n, int flicker_ms,
                    int cooldown_ms) {

  // Display the value on the digit for some time
  // enable current sink pins as appropriate
  if (digits_mask & 0x1) {
    digitalWrite(pin_digit2, LOW);
  }
  if (digits_mask & 0x2) {
    digitalWrite(pin_digit1, LOW);
  }
  shift_out(shift_values, n);
  delay(flicker_ms);

  // Allow a cooldown period for that LED set to turn off
  shift_out(0, n);
  delay(cooldown_ms);

  // Clear the display
  shift_out(0x00, n);
  digitalWrite(pin_digit1, HIGH);
  digitalWrite(pin_digit2, HIGH);
}

/* ============================ Compass ============================ */

void setup_compass(void) {
  /* Setup the compass i2c peripheral. */
  Wire.begin();
  compass.init();
  compass.enableDefault();

  // Calibrate the compass using max/min values for each axis.
  /* min: {  -977,  -1092,   -722}    max: {  +819,   +801,  +1031} */
  /*  min: { -1023,   -327,    +82}    max: {  -292,   +106,   +156} */

  // Calibrated after new PCB:
  // min: {  -218,   -240,   -111}    max: {  +293,   +234,    +31}

  compass.m_min = (LSM303::vector<int16_t>){-218, -240, -111};
  compass.m_max = (LSM303::vector<int16_t>){+293, +234, +31};
}

unsigned long get_heading_chars(float heading) {

  int heading_offset = (int)heading + 22.5 + compass_offset_deg;
  Serial.print(heading_offset);
  int i = 0;

  while (heading_offset < 0) {
    heading_offset += 360;
  }

  while (heading_offset > 45) {
    i++;
    heading_offset -= 45;
  }
  Serial.print(" is heading: ");
  Serial.println(i % 8);
  return (long)compass_rosette[i % 8];
}

/* Shift register helper functions */

void digitalWriteDelay(int pin, int value) {
  digitalWrite(pin, value);
  delay(10);
}