/* Contains useful values and constants for working with the LSM303DLHC
   combination accelerometer/magnetometer. I am using it as a
   compass.
   */

#ifndef LSM303DLHC_H

/* I2C addresses */
#define ACCELEROMETER_I2C_ADDR 0b0011001
#define MAGNETOMETER_I2C_ADDR 0b0011110

/* Read and write bits */
/* These get masked with addresses to define read/write */
#define I2C_WRITE 0
#define I2C_READ (1 << 7)

/* Config addresses and desired values */

/* WHO_AM_I register */
#define REGISTER_WHOAMI 0x0F

/* MAG CONFIG register */
#define REGISTER_MAG_CONFIG 0x60

#define REGISTER_MAG_CONFIG_TEMP_COMP (0x1 << 0) // enable temp compensation
#define REGISTER_MAG_CONFIG_REBOOT (0x1 << 1)    // reboot memory content
#define REGISTER_MAG_CONFIG_RESET (0x1 << 2)     // do a soft reset
#define REGISTER_MAG_CONFIG_LOW_POWER (0x1 << 3) // enable low power mode
#define REGISTER_MAG_CONFIG_RATE (0x3 << 4)      // output data rate bits
#define REGISTER_MAG_CONFIG_MODE (0x3 << 6)      // Mode select bits

/* And here is the value we want in that register */
#define REGISTER_MAG_CONFIG_SETTINGS (REGISTER_MAG_CONFIG_TEMP_COMP)

#define LSM303DLHC_H
#endif