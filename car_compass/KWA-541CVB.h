/* Contains useful values for working with the  the KWA-541CVB
   KWA-541CVB is a common-negative 14 segment LED display. Datasheet is in
   the datasheets folder.
  */

#ifndef KWA_541CVB_H

/* Definitions for each LED pin on the display: */
#define LED_SEG_A (1 << 0)
#define LED_SEG_B (1 << 1)
#define LED_SEG_C (1 << 2)
#define LED_SEG_D (1 << 3)
#define LED_SEG_E (1 << 4)
#define LED_SEG_F (1 << 5)
#define LED_SEG_G1 (1 << 6)
#define LED_SEG_G2 (1 << 7)
#define LED_SEG_H (1 << 8)
#define LED_SEG_J (1 << 9)
#define LED_SEG_K (1 << 10)
#define LED_SEG_L (1 << 11)
#define LED_SEG_M (1 << 12)
#define LED_SEG_N (1 << 13)
#define LED_SEG_DP (1 << 14)

/* Definitions for combinations of characters */
#define LED_CHAR_N                                                             \
  (LED_SEG_F | LED_SEG_E | LED_SEG_H | LED_SEG_N | LED_SEG_C |                 \
   LED_SEG_B) // F E H N C B
#define LED_CHAR_E                                                             \
  (LED_SEG_A | LED_SEG_F | LED_SEG_G1 | LED_SEG_G2 | LED_SEG_E |               \
   LED_SEG_D) // A F G1 G2 E D
#define LED_CHAR_S                                                             \
  (LED_SEG_A | LED_SEG_F | LED_SEG_G1 | LED_SEG_G2 | LED_SEG_C |               \
   LED_SEG_D) // A F G1 G2 C D
#define LED_CHAR_W                                                             \
  (LED_SEG_F | LED_SEG_E | LED_SEG_L | LED_SEG_N | LED_SEG_C |                 \
   LED_SEG_B) // F E L N C B

/* Need to multiply by 65536 because bitshift operations return an int */
const unsigned long compass_rosette[8] = {(LED_CHAR_N * 65536), // north
                                          (LED_CHAR_N * 65536) +
                                              LED_CHAR_E,       // northeast
                                          (LED_CHAR_E * 65536), //...
                                          (LED_CHAR_S * 65536) + LED_CHAR_E,
                                          LED_CHAR_S * 65536,
                                          (LED_CHAR_S * 65536) + LED_CHAR_W,
                                          LED_CHAR_W * 65536,
                                          (LED_CHAR_N * 65536) + LED_CHAR_W};

#define KWA_541CVB_H
#endif